package com.emirilda.spigotmc.cornerstones.events.griefprotection;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import lombok.extern.java.Log;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;

import java.util.ArrayList;
import java.util.List;

@Log
public class OtherBlockEvents implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void physics(BlockPhysicsEvent event) {
        String changeId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
        String sourceId = DatabaseSerializationUtility.serializeChunkLocation(event.getSourceBlock().getChunk());
        if (Main.getCornerstones().containsKey(changeId) || Main.getCornerstones().containsKey(sourceId)) {
            if (!changeId.equals(sourceId)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void spread(BlockSpreadEvent event) {
        String changeId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
        String sourceId = DatabaseSerializationUtility.serializeChunkLocation(event.getSource().getChunk());
        if (Main.getCornerstones().containsKey(changeId) || Main.getCornerstones().containsKey(sourceId)) {
            if (!changeId.equals(sourceId)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void fromTo(BlockFromToEvent event) {
        String changeId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
        String sourceId = DatabaseSerializationUtility.serializeChunkLocation(event.getToBlock().getChunk());
        if (Main.getCornerstones().containsKey(changeId) || Main.getCornerstones().containsKey(sourceId)) {
            if (!changeId.equals(sourceId)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void burn(BlockBurnEvent event) {
        if (event.getIgnitingBlock() == null) return;
        String changeId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
        String sourceId = DatabaseSerializationUtility.serializeChunkLocation(event.getIgnitingBlock().getChunk());
        if (Main.getCornerstones().containsKey(changeId) || Main.getCornerstones().containsKey(sourceId)) {
            if (!changeId.equals(sourceId)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void explode(BlockExplodeEvent event) {
        if (event.blockList().isEmpty()) return;
        String sourceId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());

        List<Block> newBlockList = new ArrayList<>();

        if (Main.getCornerstones().containsKey(sourceId)) {
            event.blockList().forEach(block -> {
                String blockId = DatabaseSerializationUtility.serializeChunkLocation(block.getChunk());
                boolean isOkay = sourceId.equals(blockId);
                if (isOkay && block.getY() > Main.getCornerstones().get(sourceId).getBaseLevel() - 1)
                    newBlockList.add(block);
            });
        } else {
            event.blockList().forEach(block -> {
                String blockId = DatabaseSerializationUtility.serializeChunkLocation(block.getChunk());
                boolean isOkay = true;
                if (Main.getCornerstones().containsKey(blockId)) isOkay = blockId.equals(sourceId);
                if (isOkay && CornerstoneUtility.getOutsideLocations().stream().noneMatch(location ->
                        CornerstoneUtility.compareLocation(location, block.getLocation()))) newBlockList.add(block);
            });
        }

        event.blockList().clear();
        event.blockList().addAll(newBlockList);
    }

}
