package com.emirilda.spigotmc.cornerstones.events.griefprotection;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import lombok.extern.java.Log;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.world.StructureGrowEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log
public class GrowthEvents implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void grow(BlockGrowEvent event){
        if(event.getNewState().getType() != Material.MELON && event.getNewState().getType() != Material.PUMPKIN) return;
        Arrays.stream(BlockFace.values()).forEach(blockFace -> {
            Block relativeBlock = event.getBlock().getRelative(blockFace);
            if(relativeBlock.getType() == Material.MELON_STEM || relativeBlock.getType() == Material.PUMPKIN_STEM){

                String sourceChunkId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
                String relativeChunkId = DatabaseSerializationUtility.serializeChunkLocation(relativeBlock.getChunk());
                if (Main.getCornerstones().containsKey(sourceChunkId) || Main.getCornerstones().containsKey(relativeChunkId)) {
                    if (!sourceChunkId.equals(relativeChunkId)) {
                        event.setCancelled(true);
                    }
                }
            }
        });
    }

    @EventHandler(ignoreCancelled = true)
    public void structureGrow(StructureGrowEvent event) {
        if (event.getBlocks().isEmpty()) return;
        String sourceId = DatabaseSerializationUtility.serializeChunkLocation(event.getLocation().getChunk());

        List<BlockState> newBlockList = new ArrayList<>();

        if (Main.getCornerstones().containsKey(sourceId)) {
            event.getBlocks().forEach(block -> {
                String blockId = DatabaseSerializationUtility.serializeChunkLocation(block.getChunk());
                boolean isOkay = sourceId.equals(blockId);
                if (isOkay && block.getY() > Main.getCornerstones().get(sourceId).getBaseLevel() - 1)
                    newBlockList.add(block);
            });
        } else {
            event.getBlocks().forEach(block -> {
                String blockId = DatabaseSerializationUtility.serializeChunkLocation(block.getChunk());
                boolean isOkay = true;
                if (Main.getCornerstones().containsKey(blockId)) isOkay = blockId.equals(sourceId);
                if (isOkay && CornerstoneUtility.getOutsideLocations().stream().noneMatch(location ->
                        CornerstoneUtility.compareLocation(location, block.getLocation()))) newBlockList.add(block);
            });
        }

        event.getBlocks().clear();
        event.getBlocks().addAll(newBlockList);
    }

}

