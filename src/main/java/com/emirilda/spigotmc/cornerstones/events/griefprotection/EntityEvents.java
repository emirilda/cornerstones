package com.emirilda.spigotmc.cornerstones.events.griefprotection;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Cornerstone;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import lombok.extern.java.Log;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Log
public class EntityEvents implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void interact(PlayerInteractEntityEvent event) {

        String chunkId = DatabaseSerializationUtility.serializeChunkLocation(event.getRightClicked().getLocation().getChunk());
        if (!Main.getCornerstones().containsKey(chunkId)) return;

        Cornerstone cornerstone = Main.getCornerstones().get(chunkId);

        if(!cornerstone.isClaimed()){
            event.setCancelled(true);
            return;
        }

        assert cornerstone.getClaimedData() != null;
        if(CornerstoneUtility.canBuild(event.getPlayer().getUniqueId(), cornerstone.getClaimedData())){
            event.setCancelled(true);
            return;
        }

        if(event.getRightClicked().getLocation().getY() < cornerstone.getBaseLevel())
            event.setCancelled(true);
        else if(event.getRightClicked().getLocation().getY() > CornerstoneUtility.getBuildLimit()) {
            event.setCancelled(true);
        }

    }

    @EventHandler(ignoreCancelled = true)
    public void damage(EntityDamageByEntityEvent event){
        if(event.getDamager() instanceof Player player){

            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(event.getEntity().getLocation().getChunk());
            if(!Main.getCornerstones().containsKey(chunkId)) return;

            Cornerstone cornerstone = Main.getCornerstones().get(chunkId);

            if(!cornerstone.isClaimed()){
                event.setCancelled(true);
                return;
            }

            assert cornerstone.getClaimedData() != null;
            if(CornerstoneUtility.canBuild(player.getUniqueId(), cornerstone.getClaimedData())){
                event.setCancelled(true);
                return;
            }

            if (event.getEntity().getLocation().getY() < cornerstone.getBaseLevel())
                event.setCancelled(true);
            else if (event.getEntity().getLocation().getY() > CornerstoneUtility.getBuildLimit()) {
                event.setCancelled(true);
            }

        } else {
            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(event.getEntity().getLocation().getChunk());
            if (!Main.getCornerstones().containsKey(chunkId)) return;
            if (!chunkId.equals(DatabaseSerializationUtility.serializeChunkLocation(event.getDamager().getLocation().getChunk())))
                event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void changeBlock(EntityChangeBlockEvent event) {
        String changeId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
        String sourceId = DatabaseSerializationUtility.serializeChunkLocation(event.getEntity().getLocation().getChunk());
        if (Main.getCornerstones().containsKey(changeId) || Main.getCornerstones().containsKey(sourceId)) {
            if (!changeId.equals(sourceId)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void ignite(EntitySpawnEvent event) {
        if (event.getEntity().getType() == EntityType.PRIMED_TNT) {
            primedTnTs.put(event.getEntity().getUniqueId(), DatabaseSerializationUtility.serializeChunkLocation(event.getLocation().getChunk()));
        }
    }

    private final HashMap<UUID, String> primedTnTs = new HashMap<>();

    @EventHandler(ignoreCancelled = true)
    public void explode(EntityExplodeEvent event) {
        if (event.blockList().isEmpty()) return;
        String sourceId;

        if (event.getEntityType() == EntityType.PRIMED_TNT) {
            sourceId = primedTnTs.get(event.getEntity().getUniqueId());
            primedTnTs.remove(event.getEntity().getUniqueId());
        } else
            sourceId = DatabaseSerializationUtility.serializeChunkLocation(event.getLocation().getChunk());

        List<Block> newBlockList = new ArrayList<>();

        if (Main.getCornerstones().containsKey(sourceId)) {
            event.blockList().forEach(block -> {
                String blockId = DatabaseSerializationUtility.serializeChunkLocation(block.getChunk());
                boolean isOkay = sourceId.equals(blockId);
                if (isOkay && block.getY() > Main.getCornerstones().get(sourceId).getBaseLevel() - 1)
                    newBlockList.add(block);
            });
        } else {
            if (Main.getCornerstones().size() != 0 && CornerstoneUtility.getOutsideLocations().size() <= 0)
                CornerstoneUtility.updateOutsideLocation();
            event.blockList().forEach(block -> {
                String blockId = DatabaseSerializationUtility.serializeChunkLocation(block.getChunk());
                boolean isOkay = true;
                if (Main.getCornerstones().containsKey(blockId)) isOkay = blockId.equals(sourceId);
                if (isOkay && CornerstoneUtility.getOutsideLocations().stream().noneMatch(location ->
                        CornerstoneUtility.compareLocation(location, block.getLocation()))) newBlockList.add(block);
            });
        }

        event.blockList().clear();
        event.blockList().addAll(newBlockList);
    }

}
