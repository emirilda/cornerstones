package com.emirilda.spigotmc.cornerstones.events;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.managers.GuiManager;
import com.emirilda.spigotmc.cornerstones.managers.SavesManager;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.cornerstones.utility.Settings;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.ClaimedCornerstoneData;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Cornerstone;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.User;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.datablocks.DataBlock;
import lombok.extern.java.Log;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.ArrayList;
import java.util.Objects;

@Log
public class SignInteractEvent implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void click(PlayerInteractEvent event) {

        if (event.getClickedBlock() == null || event.getClickedBlock().getType() == Material.AIR) return;
        if (event.getHand() != EquipmentSlot.HAND) return;

        if (Main.getDataBlockManager().isDataBlock(event.getClickedBlock())) {

            DataBlock dataBlock = Main.getDataBlockManager().getDataBlock(event.getClickedBlock());

            if (dataBlock.contains("unbreakable") && dataBlock.getBoolean("unbreakable")) event.setCancelled(true);
            if (!dataBlock.contains("sign") && !dataBlock.contains("options")) return;

            Player player = event.getPlayer();
            String chunkId = dataBlock.getString("chunk");
            Cornerstone cornerstone = Main.getCornerstones().get(chunkId);

            if (dataBlock.contains("sign") && dataBlock.getBoolean("sign")) {

                if (cornerstone.isClaimed()) {
                    if (cornerstone.getClaimedData() != null)
                        player.sendMessage(Messages.USAGE_CLAIMED_BY.get(player, Objects.requireNonNull(Main.getInstance().getServer().getPlayer(cornerstone.getClaimedData().getOwner())).getName()));
                    return;
                }

                if (Main.getPlayerCooldowns().contains(player.getUniqueId())) {
                    player.sendMessage(Messages.USAGE_COOLDOWN.get(player));
                    return;
                }

                User user = Main.getUsers().get(player.getUniqueId());

                if (!user.getCornerstoneChunkId().isEmpty()) {
                    CornerstoneUtility.unclaimChunk(player, user);
                }

                cornerstone.setClaimed(true);
                Main.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(Main.getInstance(), () -> {

                    Chunk chunk = DatabaseSerializationUtility.deserializeChunkLocation(dataBlock.getString("chunk")).getChunk();
                    user.setCornerstoneChunkId(chunkId);

                    log.info("Claiming cornerstone for " + player.getName() + " at " + chunk.getX() + ", " + chunk.getZ());

                    //if this is null, that means they have no saved structure, and we shall create a new one.
                    ClaimedCornerstoneData claimedCornerstoneData = SavesManager.getClaimedData(player.getUniqueId(), user.getActingCornerstone());

                    if (claimedCornerstoneData == null) {
                        claimedCornerstoneData = new ClaimedCornerstoneData(player.getUniqueId(), new ArrayList<>(), false, user.getActingCornerstone());
                        SavesManager.placeStarterStructure(chunk, cornerstone.getBaseLevel());
                    } else {
                        SavesManager.placePlayerStructure(chunk, cornerstone.getBaseLevel(), player.getUniqueId(), user.getActingCornerstone());
                    }
                    dataBlock.getBlock().setType(Material.PLAYER_HEAD);
                    Skull head = (Skull) dataBlock.getBlock().getState();
                    head.setOwningPlayer(player);
                    head.update();
                    cornerstone.setClaimedData(claimedCornerstoneData);
                    player.sendMessage(Messages.USAGE_CLAIMED.get(player));
                }, 10);

                /* Add user to cooldown */
                Main.getPlayerCooldowns().add(player.getUniqueId());
                Main.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(Main.getInstance(),
                        () -> Main.getPlayerCooldowns().remove(player.getUniqueId()), ((long) Settings.CLAIMING_COOLDOWN.getInt() * 60 * 20));

            } else if (dataBlock.contains("options") && dataBlock.getBoolean("options")) {
                if(cornerstone.isClaimed() && cornerstone.getClaimedData() != null) {
                    if (cornerstone.getClaimedData().getOwner() == player.getUniqueId())
                        GuiManager.openInventory(player, Main.getInventories().get("main"), cornerstone.getClaimedData());
                }
            }
        }
    }
}
