package com.emirilda.spigotmc.cornerstones.events.connection;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.managers.SavesManager;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.User;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionEvents implements Listener {

    @EventHandler
    public void join(PlayerJoinEvent event){
        Main.getUsers().put(event.getPlayer().getUniqueId(), new User("", SavesManager.getActingCornerstone(event.getPlayer().getUniqueId())));
    }

    @EventHandler
    public void leave(PlayerQuitEvent event){

        User user = Main.getUsers().get(event.getPlayer().getUniqueId());

        if (!user.getCornerstoneChunkId().isEmpty()) {
            CornerstoneUtility.unclaimChunk(event.getPlayer(), user);
        }

        Main.getUsers().remove(event.getPlayer().getUniqueId());
        Main.getUserQueue().remove(event.getPlayer().getUniqueId());
    }
}
