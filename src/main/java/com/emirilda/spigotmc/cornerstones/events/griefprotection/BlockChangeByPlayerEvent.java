package com.emirilda.spigotmc.cornerstones.events.griefprotection;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Cornerstone;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class BlockChangeByPlayerEvent implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void interact(PlayerInteractEvent event){
        if(event.getClickedBlock() == null || event.getClickedBlock().getType() == Material.AIR) return;
        if(Main.getCornerstones().containsKey(DatabaseSerializationUtility.serializeChunkLocation(event.getClickedBlock().getChunk()))){

            Chunk chunk = event.getClickedBlock().getChunk();
            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(chunk);
            Cornerstone cornerstone = Main.getCornerstones().get(chunkId);
            if(!cornerstone.isClaimed()) {
                event.setCancelled(true);
                return;
            }

            assert cornerstone.getClaimedData() != null;
            if(cornerstone.getClaimedData().isRestricted()){
                event.setCancelled(true);
                return;
            }

            if(CornerstoneUtility.canBuild(event.getPlayer().getUniqueId(), cornerstone.getClaimedData())){
                event.setCancelled(true);
                return;
            }

            if(event.getClickedBlock().getY() < cornerstone.getBaseLevel()+(event.getAction() == Action.RIGHT_CLICK_BLOCK ? -1 : 0))
                event.setCancelled(true);
            else if(event.getClickedBlock().getY() > CornerstoneUtility.getBuildLimit()){
                event.setCancelled(true);
                event.getPlayer().sendMessage(Messages.USAGE_BUILD_LIMIT.get(event.getPlayer()));
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void blockPlace(BlockPlaceEvent event){
        if(Main.getCornerstones().containsKey(DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk()))){

            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk());
            Cornerstone cornerstone = Main.getCornerstones().get(chunkId);
            if(!cornerstone.isClaimed()) {
                event.setCancelled(true);
                return;
            }

            assert cornerstone.getClaimedData() != null;
            if(cornerstone.getClaimedData().isRestricted()){
                event.setCancelled(true);
                return;
            }

            if(CornerstoneUtility.canBuild(event.getPlayer().getUniqueId(), cornerstone.getClaimedData())){
                event.setCancelled(true);
                return;
            }

            if(event.getBlock().getY() < cornerstone.getBaseLevel())
                event.setCancelled(true);
            else if(event.getBlock().getY() > CornerstoneUtility.getBuildLimit()) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(Messages.USAGE_BUILD_LIMIT.get(event.getPlayer()));
            }

        }
    }

    @EventHandler(ignoreCancelled = true)
    public void blockBreak(BlockBreakEvent event){
        if(Main.getCornerstones().containsKey(DatabaseSerializationUtility.serializeChunkLocation(event.getBlock().getChunk()))){

            Chunk chunk = event.getBlock().getChunk();
            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(chunk);
            Cornerstone cornerstone = Main.getCornerstones().get(chunkId);
            if(!cornerstone.isClaimed()) {
                event.setCancelled(true);
                return;
            }

            assert cornerstone.getClaimedData() != null;
            if(cornerstone.getClaimedData().isRestricted()){
                event.setCancelled(true);
                return;
            }

            if(CornerstoneUtility.canBuild(event.getPlayer().getUniqueId(), cornerstone.getClaimedData())){
                event.setCancelled(true);
                return;
            }

            if(event.getBlock().getY() < cornerstone.getBaseLevel())
                event.setCancelled(true);
            else if(event.getBlock().getY() > CornerstoneUtility.getBuildLimit()) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(Messages.USAGE_BUILD_LIMIT.get(event.getPlayer()));
            }
        }
    }

}