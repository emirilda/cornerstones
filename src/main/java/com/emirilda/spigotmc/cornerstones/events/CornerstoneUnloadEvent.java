package com.emirilda.spigotmc.cornerstones.events;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.CornerstoneUtility;
import com.emirilda.spigotmc.cornerstones.utility.Settings;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Cornerstone;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.extern.java.Log;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

import java.util.Objects;

@Log
public class CornerstoneUnloadEvent implements Listener {

    @EventHandler
    public void load(ChunkUnloadEvent event) {
        if (Settings.REMOVE_ON_UNLOAD.getBoolean())
            try {
                if (Main.getCornerstones().containsKey(DatabaseSerializationUtility.serializeChunkLocation(event.getChunk()))) {
                    Cornerstone cornerstone = Main.getCornerstones().get(DatabaseSerializationUtility.serializeChunkLocation(event.getChunk()));
                    if (cornerstone.isClaimed()) {
                        assert cornerstone.getClaimedData() != null;
                        Player player = Main.getInstance().getServer().getPlayer(cornerstone.getClaimedData().getOwner());

                        CornerstoneUtility.unclaimChunk(Objects.requireNonNull(player), Main.getUsers().get(player.getUniqueId()));
                    }
                }
            } catch (Exception exception) {
                MessageUtility.logException(exception, getClass(), log);
            }
    }

}
