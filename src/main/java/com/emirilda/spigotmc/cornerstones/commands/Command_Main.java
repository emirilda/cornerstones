package com.emirilda.spigotmc.cornerstones.commands;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.emirilda.commands.builder.CommandHookExecutor;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import org.bukkit.command.CommandSender;

@SuppressWarnings("unused")
public class Command_Main {

    @CommandHookExecutor("cornerstones_info")
    public void sendInfo(CommandSender sender){
        pluginInfo(sender);
    }

    @CommandHookExecutor("cornerstones_reload")
    public void reload(CommandSender sender){
        Main.reload();
        sender.sendMessage(Messages.RELOADED.get(null, Main.getPluginName(), Main.getPluginVersion()));
    }


    private void pluginInfo(CommandSender sender) {
        sender.sendMessage(MessageUtility.colorizeMessage(null, String.format("""

                        &5&l~~~~~~~~~~~~~~~~~~~~
                        &d%s v%s
                        &d%s
                        &dDeveloped by %s from Emirilda
                        &l
                        &5&l~~~~~~~~~~~~~~~~~~~~
                        """,
                Main.getPluginName(), Main.getPluginVersion(),
                Main.getPluginDescription(),
                Main.getPluginAuthors()
        )));
    }

}
