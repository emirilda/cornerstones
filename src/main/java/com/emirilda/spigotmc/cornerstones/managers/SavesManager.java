package com.emirilda.spigotmc.cornerstones.managers;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.utility.Settings;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.ClaimedCornerstoneData;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Cornerstone;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.io.ByteStreams;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.UtilityClass;
import lombok.extern.java.Log;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.structure.Mirror;
import org.bukkit.block.structure.StructureRotation;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.structure.Structure;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

import static com.emirilda.spigotmc.emirilda.database.DatabaseValues.*;

@UtilityClass
@Log
public class SavesManager {

    private static final String
            chunksTable = "cornerstones_chunks",
            playersTable = "cornerstones_players",
            cornerstonesTable = "cornerstones_cornerstones";

    /*

    data:
      structures:
        savedChunks:
          server1:
            structure.nbt
          server2:
            structure.nbt
        players:
          3liass:
            cornerstone_1.nbt
            cornerstone_2.nbt
           Slimee__:
            cornerstone_1.nbt


     */

    @Getter
    private static String datafolder;

    public static void init(){
        try {
            //Chunks table
            Main.getDatabaseManager().createTable(chunksTable,
                    "`chunk` " + VARCHAR.get(250) + PRIMARY_KEY.get() +
                             ", `level` " + SMALLINT.get() +
                             ", `server` " + VARCHAR.get(200)
            );

            //Players table
            Main.getDatabaseManager().createTable(playersTable,
                    "`uuid` " + VARCHAR.get(200) + PRIMARY_KEY.get() +
                             ", `name` " + VARCHAR.get(200) +
                             ", `acting_cornerstone` " + SMALLINT.get()
            );

            //Cornerstones table
            Main.getDatabaseManager().createTable(cornerstonesTable,
                    "`keyid` " + VARCHAR.get(200) + PRIMARY_KEY.get() +
                             ", `owner` " + VARCHAR.get(200) +
                             ", `id` " + SMALLINT.get() +
                             ", `trusted` " + MEDIUMINT.get()
            );


            //Getting path for datafolder
            datafolder = Settings.DATAFOLDER.getString().replace("{pluginFolder}", Main.getInstance().getDataFolder().getPath());
            datafolder+= "/data";


            Options.init();

        }catch (Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
        }
    }

    public static ClaimedCornerstoneData getClaimedData(UUID owner, int id){
        try{

            ResultSet result = Main.getDatabaseManager().executeQuery("SELECT * FROM `" + cornerstonesTable + "` WHERE `keyid`=\"" + owner + "-" + id + "\";");

            if(!result.next()) return null;

            String trustedRaw = result.getString("trusted");
            List<UUID> trustedUsers;
            if(Strings.isNullOrEmpty(trustedRaw)) trustedUsers = new ArrayList<>();
            else trustedUsers = Arrays.stream(trustedRaw.split(",")).map(UUID::fromString).collect(Collectors.toList());

            return new ClaimedCornerstoneData(owner, trustedUsers, false, id);

        }catch (Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
            return null;
        }
    }

    public static void saveClaimedData(ClaimedCornerstoneData data){
        if(Main.getDatabaseManager().isSqlite()){
            Main.getDatabaseManager().execute("INSERT OR REPLACE INTO `" + cornerstonesTable + "` (keyid, owner, id, trusted) VALUES (" +
                            "\"" + data.getOwner() + "-" + data.getId() + "\", " +
                            "\"" + data.getOwner() + "\", " +
                            "\"" + data.getId() + "\", " +
                            "\"" + Joiner.on(',').join(data.getTrustedUsers()) + "\");"
                    //"\"" + StringUtils.join(data.getTrustedUsers(), ',') + "\");"
            );
        }else{
            Main.getDatabaseManager().execute("INSERT INTO `" + cornerstonesTable + "` (keyid, owner, id, trusted) VALUES (" +
                            "\"" + data.getOwner() + "-" + data.getId() + "\", " +
                            "\"" + data.getOwner() + "\", " +
                            "\"" + data.getId() + "\", " +
                            "\"" + Joiner.on(',').join(data.getTrustedUsers()) + "\") " +
                            //"\"" + StringUtils.join(data.getTrustedUsers(), ',') + "\") " +
                            "ON DUPLICATE KEY UPDATE " +
                            "`trusted` = \"" + Joiner.on(',').join(data.getTrustedUsers()) + "\";"
                    //"`trusted` = \"" + StringUtils.join(data.getTrustedUsers(), ',') + "\";"
            );
        }
    }

    public static void saveUser(UUID uuid, String name, int actingCornerstone){
        if(Main.getDatabaseManager().isSqlite()){
            Main.getDatabaseManager().execute("INSERT OR REPLACE INTO `" + playersTable + "` (uuid, name, acting_cornerstone) VALUES (" +
                    "\"" + uuid + "\", " +
                    "\"" + name + "\", " +
                    "\"" + actingCornerstone + "\");"
            );
        }else{
            Main.getDatabaseManager().execute("INSERT INTO `" + playersTable + "` (uuid, name, acting_cornerstone) VALUES (" +
                    "\"" + uuid + "\", " +
                    "\"" + name + "\", " +
                    "\"" + actingCornerstone + "\") " +
                    "ON DUPLICATE KEY UPDATE " +
                    "`name` = \"" + name + "\", " +
                    "`acting_cornerstone` = \"" + actingCornerstone + "\";"
            );
        }
    }

    public static int getActingCornerstone(UUID user){
        try{

            ResultSet result = Main.getDatabaseManager().executeQuery("SELECT * FROM `" + playersTable + "` WHERE `uuid`=\"" + user + "\";");

            if(!result.next())
                return 1;

            return result.getInt("acting_cornerstone");

        }catch (Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
            return 1;
        }
    }

    public static boolean registerChunk(Chunk chunk, int level){
        try {
            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(chunk);
            String server = Settings.SERVER.getString();
            chunkId += "-" + server;

            ResultSet result = Main.getDatabaseManager().executeQuery("SELECT * FROM `" + chunksTable + "` WHERE `chunk`=\"" + chunkId + "\";");

            if(result.next())
                return false;


            Main.getDatabaseManager().execute("INSERT INTO `" + chunksTable + "` (chunk, level, server) VALUES (" +
                    "\"" + chunkId + "\", " +
                    "\"" + level + "\", " +
                    "\"" + server + "\");");

            /* Creating and saving structure */
            int x = chunk.getX() * 16;
            int y = -64;
            int z = chunk.getZ() * 16;

            int dx = x + 16;
            int dy = 319;
            int dz = z + 16;

            Location corner1 = chunk.getWorld().getBlockAt(x, y, z).getLocation();
            Location corner2 = chunk.getWorld().getBlockAt(dx, dy, dz).getLocation();

            Structure structure = Main.getStructureManager().createStructure();

            structure.fill(corner1, corner2, true);

            File file = new File(datafolder, "structures/savedChunks/" + Settings.SERVER.getString() + "/" + chunkId + ".nbt");
            boolean check = true;
            if (file.exists()) check = file.delete();
            check = check && (file.getParentFile().mkdirs() || file.createNewFile());
            log.info("Registering chunk (" + chunk.getX() + ", " + chunk.getZ() + ") as cornerstone chunk (at level " + level + ")\nSaving old chunk in file: " + file.getPath());
            Main.getStructureManager().saveStructure(file, structure);

            return check;
        }catch(Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
            return false;
        }
    }

    public static boolean unregisterChunk(Chunk chunk){
        try{

            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(chunk);
            String server = Settings.SERVER.getString();
            chunkId += "-" + server;

            ResultSet result = Main.getDatabaseManager().executeQuery("SELECT * FROM `" + chunksTable + "` WHERE `chunk`=\"" + chunkId + "\";");

            if(!result.next())
                return false;

            File file = new File(datafolder, "structures/savedChunks/"+Settings.SERVER.getString()+"/"+chunkId+".nbt");
            Structure structure = Main.getStructureManager().loadStructure(file);
            Location location = chunk.getWorld().getBlockAt(chunk.getX()*16, -64, chunk.getZ()*16).getLocation();
            structure.place(location, true, StructureRotation.NONE, Mirror.NONE, 0, 1, new Random());
            boolean check = file.delete();

            Main.getDatabaseManager().execute("DELETE FROM `" + chunksTable + "` WHERE `chunk`=\"" + chunkId + "\";");

            return check;

        }catch(Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
            return false;
        }
    }

    public static boolean isRegisteredChunk(Chunk chunk){
        try {
            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(chunk);
            String server = Settings.SERVER.getString();
            chunkId += "-" + server;

            ResultSet result = Main.getDatabaseManager().executeQuery("SELECT * FROM `" + chunksTable + "` WHERE `chunk`=\"" + chunkId + "\";");

            return result.next();
        }catch (Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
            return false;
        }
    }

    public static int getChunkLevel(Chunk chunk){
        try {

            String chunkId = DatabaseSerializationUtility.serializeChunkLocation(chunk);
            String server = Settings.SERVER.getString();
            chunkId += "-" + server;

            ResultSet result = Main.getDatabaseManager().executeQuery("SELECT * FROM `" + chunksTable + "` WHERE `chunk`=\"" + chunkId + "\";");

            return result.getInt("level");

        }catch (Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
            return -69420;
        }
    }

    private static void saveStructure(Chunk chunk, int y, String destination){

        try {
            File file = new File(getDatafolder(), destination);
            if(!file.exists()) {
                boolean check = file.getParentFile().mkdirs() || file.createNewFile();
                if (!check) return;
            }

            int x = chunk.getX() * 16;
            int z = chunk.getZ() * 16;

            int dx = x + 16;
            int dy = y + Settings.CORNERSTONES_HEIGHT.getInt();
            int dz = z + 16;

            Location corner1 = chunk.getWorld().getBlockAt(x, y, z).getLocation();
            Location corner2 = chunk.getWorld().getBlockAt(dx, dy, dz).getLocation();

            Structure structure = Main.getStructureManager().createStructure();

            structure.fill(corner1, corner2, true);

            Main.getStructureManager().saveStructure(file, structure);
        }catch(Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
        }
    }

    public static void saveUnclaimedStructure(Chunk chunk, int level){
        saveStructure(chunk, level, "options/unclaimed.nbt");
    }

    public static void saveStarterStructure(Chunk chunk, int level){
        saveStructure(chunk, level, "options/starter.nbt");
    }

    public static void savePlayerStructure(Chunk chunk, int level, UUID owner, int id){
        saveStructure(chunk, level, "structures/players/" + owner + "/" + id + ".nbt");
    }

    private static void placeStructure(Chunk chunk, int level, String path){
        try {

            File file = getOrCreateDataFile(path+".nbt");

            assert file != null;
            Structure structure = Main.getStructureManager().loadStructure(file);

            Location location = chunk.getWorld().getBlockAt(chunk.getX()*16, level, chunk.getZ()*16).getLocation();

            structure.place(location, true, StructureRotation.NONE, Mirror.NONE, 0, 1, new Random());

        }catch (Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
        }
    }

    public static void placeUnclaimedStructure(Chunk chunk, int level){
        placeStructure(chunk, level, "options/unclaimed");
    }

    public static void placeStarterStructure(Chunk chunk, int level){
        placeStructure(chunk, level, "options/starter");
    }

    public static void placePlayerStructure(Chunk chunk, int level, UUID owner, int id){
        placeStructure(chunk, level, "structures/players/" + owner + "/" + id);
     }

    private static File getOrCreateDataFile(String path){
        try{
            File file = new File(getDatafolder() + "/" + path);

            boolean check = true;
            if (!file.exists()) {
                check = file.getParentFile().mkdirs() || file.createNewFile();
            }
            if (file.length() <= 0L && check) {
                try {
                    InputStream in = Main.getInstance().getResource("data/" + file.getName());
                    try {
                        OutputStream out = new FileOutputStream(file);
                        try {
                            assert in != null;
                            ByteStreams.copy(in, out);
                            out.close();
                        } catch (Throwable throwable) {
                            try {
                                out.close();
                            } catch (Throwable throwable1) {
                                throwable.addSuppressed(throwable1);
                            }
                            throw throwable;
                        }
                        in.close();
                    } catch (Throwable throwable) {
                        if (in != null)
                            try {
                                in.close();
                            } catch (Throwable throwable1) {
                                throwable.addSuppressed(throwable1);
                            }
                        throw throwable;
                    }
                } catch (Exception exception) {
                    MessageUtility.logException(exception, Options.class, log);
                }
            }

            return file;
        }catch (Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
            return null;
        }
    }

    public static void loadAllChunks(){

        try {

            ResultSet resultSet = Main.getDatabaseManager().executeQuery("SELECT * FROM `" + chunksTable + "` WHERE `server`=\"" + Settings.SERVER.getString() + "\";");

            while (resultSet.next()) {

                String chunkId = resultSet.getString("chunk");
                chunkId = chunkId.substring(0, (chunkId.length()-(Settings.SERVER.getString().length()+1)));

                int level = resultSet.getInt("level");

                Main.getCornerstones().put(chunkId, new Cornerstone(level, false, null));

            }

        }catch (Exception exception){
            MessageUtility.logException(exception, SavesManager.class, log);
        }
    }

    @Log
    public class Options{

        private static final File file = getOrCreateDataFile("options/options.yml");
        private static final YamlConfiguration yamlConfiguration = new YamlConfiguration();

        @Getter @Setter
        private static Material base, sign, option_block;

        public static void init(){
            try {

                assert file != null;
                yamlConfiguration.load(file);

                base = Material.valueOf(yamlConfiguration.getString("base"));
                sign = Material.valueOf(yamlConfiguration.getString("sign"));
                option_block = Material.valueOf(yamlConfiguration.getString("options_block"));
            }catch (Exception exception){
                MessageUtility.logException(exception, Options.class, log);
            }
        }

        public static void saveAll(){
            try {
                yamlConfiguration.set("base", base.name());
                yamlConfiguration.set("sign", sign.name());
                yamlConfiguration.set("options_block", option_block.name());
                assert file != null;
                yamlConfiguration.save(file);
            }catch (Exception exception){
                MessageUtility.logException(exception, Options.class, log);
            }
        }
    }
}