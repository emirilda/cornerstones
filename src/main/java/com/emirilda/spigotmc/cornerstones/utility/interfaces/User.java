package com.emirilda.spigotmc.cornerstones.utility.interfaces;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor
public class User {

   private String cornerstoneChunkId;
   private int actingCornerstone;

}
