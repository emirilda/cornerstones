package com.emirilda.spigotmc.cornerstones.utility.interfaces;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.Getter;
import lombok.extern.java.Log;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

@Log
public class Gui extends InventoryGui {

    @Getter
    private final YamlInventory yamlInventory;
    private final Player player;
    @Getter
    private final HashMap<String, ItemStack> cache;

    public Gui(YamlInventory yamlInventory, ClaimedCornerstoneData claimData, Player player){
        super(yamlInventory.size(), MessageUtility.colorizeMessage(player, yamlInventory.title()), yamlInventory.updateInterval(), yamlInventory, claimData);
        this.yamlInventory = yamlInventory;
        this.player = player;
        cache = new HashMap<>();
    }

    @Override
    public void updateItems(){
        try{

            getInventory().clear();
            getActions().clear();

            if(yamlInventory.filler().isUse()){
                for (int i = 0; i < getSize(); i++) {
                    if (getItem(i) == null) {
                        setItem(i, yamlInventory.filler().createItemStack(player, getClaimData(), yamlInventory.items().values().stream().toList(), cache), ((clickPlayer, clickEvent, claimedCornerstoneData) -> clickEvent.setCancelled(true)));
                    }
                }
            }

            yamlInventory.items().forEach((key, item) -> {
                int slot = key;
                if (item.isUse() && player.hasPermission(item.getPermission())) {
                    if (item.getAction().action() == null)
                        setItem(slot, item.createItemStack(player, getClaimData(), yamlInventory.items().values().stream().toList(), cache));
                    else
                        setItem(slot, item.createItemStack(player, getClaimData(), yamlInventory.items().values().stream().toList(), cache), item.getAction().action());
                }
            });


        } catch (Exception exception) {
            MessageUtility.logException(exception, getClass(), log);
        }
    }

    @Override
    public void open (Player player) {
        updateItems();

        if(getUpdateInterval() > 0) {
            BukkitRunnable runnable = new BukkitRunnable() {
                @Override
                public void run() {
                    if (InventoryGui.getOpenInventories().containsKey(player.getUniqueId())
                            && InventoryGui.getOpenInventories().get(player.getUniqueId()) == getUuid()) {
                        updateItems();
                    } else {
                        cancel();
                    }
                }
            };

            runnable.runTaskTimer(Main.getInstance(), getUpdateInterval(), getUpdateInterval());
        }


        super.open(player);
    }

}
