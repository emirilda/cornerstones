package com.emirilda.spigotmc.cornerstones.utility.interfaces;

import com.emirilda.spigotmc.emirilda.utility.interfaces.Config;
import lombok.Getter;
import org.bukkit.configuration.MemorySection;

import java.util.HashMap;
import java.util.Set;

@SuppressWarnings("unused")
public record YamlInventory(String name,
                            int size, @Getter String title, int updateInterval,
                            YamlItem filler,
                            @Getter HashMap<Integer, YamlItem> items) {

    public static YamlInventory loadFromYaml(Config yaml, String path, String name) {
        path+=".";

        int rows = 6;
        if(yaml.get().contains(path + "rows"))
            rows = (int) yaml.get(path + "rows");

        String title = "Inventory GUI";
        if(yaml.get().contains(path + "title"))
            title = (String) yaml.get(path + "title");

        YamlItem filler = null;
        if(yaml.get().contains(path + "filler"))
            filler = YamlItem.loadFromYaml(yaml, path + "filler", "filler");

        int updateInterval = 20;
        if(yaml.get().contains(path + "updateInterval"))
            updateInterval = (int) yaml.get(path + "updateInterval");

        HashMap<Integer, YamlItem> items = new HashMap<>();

        if(yaml.get().contains(path + "items")) {
            MemorySection section = (MemorySection) yaml.get(path + "items");
            Set<String> itemPaths = section.getKeys(false);

            String finalPath = path;
            itemPaths.forEach(itemPath -> {
                YamlItem item = YamlItem.loadFromYaml(yaml, finalPath + "items." + itemPath, itemPath);
                items.put(item.getSlot(), item);
            });
        }

        return new YamlInventory(name, rows * 9, title, updateInterval, filler, items);
    }

}