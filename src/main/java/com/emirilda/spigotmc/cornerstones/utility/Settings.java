package com.emirilda.spigotmc.cornerstones.utility;

import com.emirilda.spigotmc.cornerstones.Main;

import java.util.List;

public enum Settings {

    //General
    MYSQL("mysql"),
    DATABASE_DATABASE("database.database"),
    DATABASE_HOST("database.host"),
    DATABASE_PORT("database.port"),
    DATABASE_USERNAME("database.username"),
    DATABASE_PASSWORD("database.password"),

    DEBUG("debug"),

    DATAFOLDER("datafolder"),
    SERVER("server"),

    //auto save
    AUTO_SAVE("auto_save"),
    ANNOUNCE_AUTO_SAVE("announce_auto_save"),


    //Cornerstones
    BUILD_LIMIT("cornerstones.build_limit"),
    CORNERSTONES_HEIGHT("cornerstones.height"),
    REMOVE_ON_UNLOAD("cornerstones.remove_on_unload"),
    CLAIMING_COOLDOWN("cornerstones.claiming_cooldown");

    private final String path;

    Settings(String path) {
        this.path = path;
    }

    public String getString() {
        return Main.config.get().getString(path);
    }

    public Boolean getBoolean(){
        return Main.config.get().getBoolean(path);
    }

    public int getInt(){
        return Main.config.get().getInt(path);
    }

    public List<?> getList() { return Main.config.get().getList(path); }

}
