package com.emirilda.spigotmc.cornerstones.utility.interfaces;

import com.emirilda.spigotmc.cornerstones.utility.Messages;
import com.emirilda.spigotmc.emirilda.utility.MessageUtility;
import lombok.Getter;
import lombok.extern.java.Log;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@SuppressWarnings("unused")
@Log
public class InventoryGui {

    /* Inventory UUID, Inventory Instance */
    @Getter
    private static final Map<UUID, InventoryGui> inventoriesByUUID = new HashMap<>();
    /* Player UUID, Inventory UUID; to store which players have which inventory open */
    @Getter
    private static final Map<UUID, UUID> openInventories = new HashMap<>();

    @Getter
    private final UUID uuid;
    @Getter
    private final int updateInterval;
    @Getter
    private final Inventory inventory;
    @Getter
    private final Map<Integer, GUIAction> actions;
    @Getter
    private final int size;
    @Getter
    private final GUIType type;
    @Getter
    private final String name;
    @Getter
    private Player player;
    @Getter
    private final YamlInventory yamlInventory;
    @Getter
    private final ClaimedCornerstoneData claimData;

    public InventoryGui(int size, String name, int updateInterval, YamlInventory yamlInventory, ClaimedCornerstoneData claimData) {
        uuid = UUID.randomUUID();
        this.size = size;
        this.type = GUIType.UNEDITABLE;
        this.name = name;
        this.yamlInventory = yamlInventory;
        this.claimData = claimData;
        this.updateInterval = updateInterval;
        inventory = Bukkit.createInventory(null, size, name);
        actions = new HashMap<>();
        inventoriesByUUID.put(getUuid(), this);
    }

    /* Methods for the inventory */
    public void setItem(int slot, ItemStack item, GUIAction action) {
        try {
            inventory.setItem(slot, item);
            if (action != null) actions.put(slot, action);
        } catch (Exception exception) {
            MessageUtility.logException(exception, getClass(), log);
        }
    }

    public void setItem(int slot, ItemStack item) {
        setItem(slot, item, null);
    }

    public void setItems(int[] slots, ItemStack item, GUIAction action) {
        try {
            for (int slot : slots) {
                setItem(slot, item, action);
            }
        } catch (Exception exception) {
            MessageUtility.logException(exception, getClass(), log);
        }
    }

    public void setItems(int[] slots, ItemStack item) {
        setItems(slots, item, null);
    }

    public void open(Player player) {
        try {
            this.player = player;
            player.openInventory(inventory);
            openInventories.put(player.getUniqueId(), getUuid());
        } catch (Exception exception) {
            MessageUtility.logException(exception, getClass(), log);
        }
    }

    public void updateItems() {

    }

    public void closeExecutions(Player player) {
        delete();
    }

    public void closeAll() {
        try {
            openInventories.forEach((key, value) -> {
                if (value.equals(getUuid()))
                    Objects.requireNonNull(Bukkit.getPlayer(key)).closeInventory();
            });
        } catch (Exception exception) {
            MessageUtility.logException(exception, getClass(), log);
        }
    }

    public void delete() {
        try {
            inventoriesByUUID.remove(getUuid());
        } catch (Exception exception) {
            MessageUtility.logException(exception, getClass(), log);
        }
    }

    public ItemStack getItem(int slot) {
        try {
            return getInventory().getItem(slot);
        } catch (Exception exception) {
            MessageUtility.logException(exception, getClass(), log);
            return null;
        }
    }

    /* Interface for actions (clicks) */
    public interface GUIAction {
        void click(Player player, InventoryClickEvent clickEvent, ClaimedCornerstoneData claimData);

        default void perform(Player player, InventoryClickEvent clickEvent, ClaimedCornerstoneData claimData, String permission){
            if(player.hasPermission(permission)){
                click(player, clickEvent, claimData);
            }else {
                clickEvent.setCancelled(true);
                player.sendMessage(Messages.NO_PERMISSION.get(player));
            }
        }
    }

    /* Enum for gui type */
    @SuppressWarnings("unused")
    public enum GUIType {
        EDITABLE,
        UNEDITABLE
    }

}
