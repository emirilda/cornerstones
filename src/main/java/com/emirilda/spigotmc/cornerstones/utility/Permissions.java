package com.emirilda.spigotmc.cornerstones.utility;

public enum Permissions {

    /* Basic */
    USE                                 ("cornerstones.use"),

    /* Player Commands */
    PLAYER_COMMAND_INFO                 ("cornerstones.command.player.info"),
    PLAYER_COMMAND_HELP                 ("cornerstones.command.player.help"),


    /* Admin Commands */
    ADMIN_COMMAND_RELOAD                ("cornerstones.command.admin.reload"),

    ADMIN_COMMAND_CHUNK                 ("cornerstones.command.admin.chunk"),
    ADMIN_COMMAND_CHUNK_INFO            ("cornerstones.command.admin.chunk.info"),
    ADMIN_COMMAND_CHUNK_REGISTER        ("cornerstones.command.admin.chunk.register"),
    ADMIN_COMMAND_CHUNK_UNREGISTER      ("cornerstones.command.admin.chunk.unregister"),
    ADMIN_COMMAND_CHUNK_LIST            ("cornerstones.command.admin.chunk.list"),

    ADMIN_COMMAND_CHUNK_SET             ("cornerstones.command.admin.chunk.set"),
    ADMIN_COMMAND_CHUNK_SET_BASE        ("cornerstones.command.admin.chunk.set.base"),
    ADMIN_COMMAND_CHUNK_SET_STARTER     ("cornerstones.command.admin.chunk.set.starter"),
    ADMIN_COMMAND_CHUNK_SET_UNCLAIMED   ("cornerstones.command.admin.chunk.set.unclaimed");


    private final String value;

    Permissions(String value){
        this.value = value;
    }

    public String get(){
        return this.value;
    }


}
