package com.emirilda.spigotmc.cornerstones.utility.interfaces;

public record QueueUser(QueueReasons queueReason) {

    public enum QueueReasons {
        REGISTER_CHUNK, UNREGISTER_CHUNK, SET_STARTER, SET_UNCLAIMED
    }

}
