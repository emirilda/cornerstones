package com.emirilda.spigotmc.cornerstones.utility;

import com.emirilda.spigotmc.cornerstones.Main;
import com.emirilda.spigotmc.cornerstones.managers.SavesManager;
import com.emirilda.spigotmc.cornerstones.utility.events.ChunkRegistrationFinishedEvent;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.ClaimedCornerstoneData;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.Cornerstone;
import com.emirilda.spigotmc.cornerstones.utility.interfaces.User;
import com.emirilda.spigotmc.emirilda.Emirilda;
import com.emirilda.spigotmc.emirilda.database.DatabaseSerializationUtility;
import com.emirilda.spigotmc.emirilda.datablocks.DataBlock;
import lombok.Getter;
import lombok.extern.java.Log;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.block.data.Directional;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Log
public class CornerstoneUtility {

    /* Build limit: -64 to 319 */

    @Getter
    private static boolean currentlyRegistering = false;
    @Getter
    private static final List<Location> outsideLocations = new ArrayList<>();

    public static void registerChunk(Chunk chunk, int level, Player caller) {

        currentlyRegistering = true;
        addChunkToList(chunk, level);

        int x = chunk.getX() * 16;
        int z = chunk.getZ() * 16;
        AtomicInteger y = new AtomicInteger(-64);

        BukkitRunnable runnable = new BukkitRunnable() {
            @Override
            public void run() {

                //place the blocks, layer by layer
                for (int xi = 0; xi < 16; xi++)
                    for (int zi = 0; zi < 16; zi++)
                        chunk.getWorld().getBlockAt(x + xi, y.get(), z + zi).setType((y.get() > level ? Material.AIR : SavesManager.Options.getBase()));

                y.getAndIncrement();

                if (y.get() > 319) {
                    cancel();
                    Arrays.stream(chunk.getEntities()).forEach(entity -> {
                        if (!(entity instanceof Player)) entity.remove();
                    });

                    SavesManager.placeUnclaimedStructure(chunk, level);

                    DataBlock baseBlock1 = Main.getDataBlockManager().getDataBlock(chunk.getWorld().getBlockAt(x + 8, level, z - 1));
                    DataBlock baseBlock2 = Main.getDataBlockManager().getDataBlock(chunk.getWorld().getBlockAt(x + 7, level, z - 1));

                    /* Setting the 4 outside blocks */
                    baseBlock1.setBoolean("unbreakable", true);
                    baseBlock2.setBoolean("unbreakable", true);

                    baseBlock1.setString("block", baseBlock1.getBlock().getBlockData().getAsString());
                    baseBlock2.setString("block", baseBlock2.getBlock().getBlockData().getAsString());


                    DataBlock sign = Main.getDataBlockManager().getDataBlock(chunk.getWorld().getBlockAt(x + 8, level + 1, z - 1));
                    DataBlock options = Main.getDataBlockManager().getDataBlock(chunk.getWorld().getBlockAt(x + 7, level + 1, z - 1));

                    sign.setBoolean("unbreakable", true);
                    options.setBoolean("unbreakable", true);

                    //Saving necessary information
                    String chunkId = DatabaseSerializationUtility.serializeChunkLocation(chunk);
                    sign.setString("block", sign.getBlock().getBlockData().getAsString());
                    sign.setBoolean("sign", true);
                    sign.setString("chunk", chunkId);

                    options.setString("block", options.getBlock().getBlockData().getAsString());
                    options.setBoolean("options", true);
                    options.setString("chunk", chunkId);

                    sign.getBlock().setType(Material.AIR);
                    options.getBlock().setType(Material.AIR);
                    baseBlock1.getBlock().setType(SavesManager.Options.getBase());
                    baseBlock2.getBlock().setType(SavesManager.Options.getBase());

                    //Setting block type
                    sign.getBlock().setType(SavesManager.Options.getSign());
                    options.getBlock().setType(SavesManager.Options.getOption_block());
                    if (options.getBlock().getBlockData() instanceof Directional directional) {
                        directional.setFacing(BlockFace.EAST);
                        options.getBlock().setBlockData(directional);
                    }

                    Sign signBlock = (Sign) sign.getBlock().getState();
                    org.bukkit.block.data.type.Sign signData = (org.bukkit.block.data.type.Sign) signBlock.getBlockData();
                    signData.setRotation(BlockFace.NORTH);
                    signBlock.setBlockData(signData);
                    signBlock.setEditable(false);
                    signBlock.setLine(0, Messages.SIGN_LINES_ONE.get(null));
                    signBlock.setLine(1, Messages.SIGN_LINES_TWO.get(null));
                    signBlock.setLine(2, Messages.SIGN_LINES_THREE.get(null));
                    signBlock.setLine(3, Messages.SIGN_LINES_FOUR.get(null));
                    signBlock.update();


                    ChunkRegistrationFinishedEvent chunkRegistrationFinishedEvent = new ChunkRegistrationFinishedEvent(caller, chunk);
                    Main.getInstance().getServer().getPluginManager().callEvent(chunkRegistrationFinishedEvent);

                    currentlyRegistering = false;
                }
            }
        };

        runnable.runTaskTimer(Main.getInstance(), 1, 1);

        Arrays.stream(chunk.getEntities()).forEach(entity -> {
            if (!(entity instanceof Player)) entity.remove();
        });
        updateOutsideLocation();
    }

    public static void unregisterChunk(Chunk chunk, int level) {

        int x = chunk.getX() * 16;
        int z = chunk.getZ() * 16;

        DataBlock baseBlock1 = Main.getDataBlockManager().getDataBlock(chunk.getWorld().getBlockAt(x + 8, level, z - 1));
        DataBlock baseBlock2 = Main.getDataBlockManager().getDataBlock(chunk.getWorld().getBlockAt(x + 7, level, z - 1));

        DataBlock sign = Main.getDataBlockManager().getDataBlock(chunk.getWorld().getBlockAt(x + 8, level + 1, z - 1));
        DataBlock options = Main.getDataBlockManager().getDataBlock(chunk.getWorld().getBlockAt(x + 7, level + 1, z - 1));

        //setting data
        sign.getBlock().setType(Material.AIR);
        options.getBlock().setType(Material.AIR);

        baseBlock1.getBlock().setBlockData(Bukkit.createBlockData(baseBlock1.getString("block")));
        baseBlock2.getBlock().setBlockData(Bukkit.createBlockData(baseBlock2.getString("block")));

        sign.getBlock().setBlockData(Bukkit.createBlockData(sign.getString("block")));
        options.getBlock().setBlockData(Bukkit.createBlockData(options.getString("block")));


        //clearing data
        sign.clear();
        options.clear();
        baseBlock1.clear();
        baseBlock2.clear();

        Main.getCornerstones().remove(DatabaseSerializationUtility.serializeChunkLocation(chunk));
        updateOutsideLocation();
    }

    public static void addChunkToList(Chunk chunk, int level) {
        String chunkId = DatabaseSerializationUtility.serializeChunkLocation(chunk);
        Main.getCornerstones().put(chunkId, new Cornerstone(level, false, null));
    }

    public static void unclaimChunk(Player player, User user) {
        String chunkId = user.getCornerstoneChunkId();
        Cornerstone cornerstone = Main.getCornerstones().get(chunkId);
        assert cornerstone.getClaimedData() != null;
        cornerstone.getClaimedData().setRestricted(true);
        Chunk chunk = DatabaseSerializationUtility.deserializeChunkLocation(chunkId).getChunk();

        SavesManager.saveUser(player.getUniqueId(), player.getName(), user.getActingCornerstone());
        SavesManager.saveClaimedData(cornerstone.getClaimedData());

        SavesManager.savePlayerStructure(chunk, Main.getCornerstones().get(chunkId).getBaseLevel(), player.getUniqueId(), user.getActingCornerstone());
        Arrays.stream(chunk.getEntities()).forEach(entity -> {
            if (!(entity instanceof Player)) entity.remove();
        });

        int x = chunk.getX() * 16;
        int z = chunk.getZ() * 16;
        int level = cornerstone.getBaseLevel();
        int y = level + Settings.CORNERSTONES_HEIGHT.getInt();

        while (y >= level) {
            for (int xi = 0; xi < 16; xi++)
                for (int zi = 0; zi < 16; zi++)
                    chunk.getWorld().getBlockAt(x + xi, y, z + zi).setType(Material.AIR);
            y--;
        }

        SavesManager.placeUnclaimedStructure(chunk, Main.getCornerstones().get(chunkId).getBaseLevel());

        Block block = chunk.getWorld().getBlockAt(x + 8, level + 1, z - 1);
        block.setType(SavesManager.Options.getSign());

        Sign signBlock = (Sign) block.getState();
        org.bukkit.block.data.type.Sign signData = (org.bukkit.block.data.type.Sign) signBlock.getBlockData();
        signData.setRotation(BlockFace.NORTH);
        signBlock.setBlockData(signData);
        signBlock.setEditable(false);
        signBlock.setLine(0, Messages.SIGN_LINES_ONE.get(null));
        signBlock.setLine(1, Messages.SIGN_LINES_TWO.get(null));
        signBlock.setLine(2, Messages.SIGN_LINES_THREE.get(null));
        signBlock.setLine(3, Messages.SIGN_LINES_FOUR.get(null));
        signBlock.update();

        user.setCornerstoneChunkId("");
        cornerstone.setClaimed(false);
        cornerstone.setClaimedData(null);
    }

    public static boolean isBlockedByChunk(Chunk chunk) {

        int x = chunk.getX();
        int z = chunk.getZ();

        Chunk front = chunk.getWorld().getChunkAt(x, z - 1);
        Chunk back = chunk.getWorld().getChunkAt(x, z + 1);

        return (SavesManager.isRegisteredChunk(front) || SavesManager.isRegisteredChunk(back));

    }

    public static boolean canBuild(UUID uuid, ClaimedCornerstoneData claimedCornerstoneData) {
        return (!uuid.equals(claimedCornerstoneData.getOwner()) &&
                !claimedCornerstoneData.getTrustedUsers().contains(uuid));
    }

    public static int getBuildLimit() {
        return Settings.BUILD_LIMIT.getInt() - Settings.CORNERSTONES_HEIGHT.getInt();
    }

    public static boolean compareLocation(Location first, Location second) {
        return (Objects.requireNonNull(first.getWorld()).getUID() == Objects.requireNonNull(second.getWorld()).getUID()) &&
                (first.getBlockX() == second.getBlockX()) &&
                (first.getBlockY() == second.getBlockY()) &&
                (first.getBlockZ() == second.getBlockZ());
    }

    public static void updateOutsideLocation() {
        outsideLocations.clear();
        Emirilda.getDataBlocks().forEach((s, dataBlockChunk) ->
                dataBlockChunk.getDataBlocks().forEach((s1, dataBlock) ->
                        outsideLocations.add(dataBlock.getBlock().getLocation())));
    }

}